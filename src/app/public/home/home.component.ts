import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public form: FormGroup;
  public contactList: FormArray;
  public skillList: FormArray;

  // returns all form groups under contacts
  get contactFormGroup() {
    return this.form.get('contacts') as FormArray;
  }
  get skillFormGroup() {
    return this.form.get('skills') as FormArray;
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      organization: [null, [Validators.required]],
      contacts: this.fb.array([this.createContact()]),
      skills: this.fb.array([this.createSkill()])
    });

    // set contactlist to this field
    this.contactList = this.form.get('contacts') as FormArray;
    this.skillList = this.form.get('skills') as FormArray;
  }

  // contact formgroup
  createContact(): FormGroup {
    return this.fb.group({
      type: ['email', Validators.compose([Validators.required])], // i.e Email, Phone
      name: [null, Validators.compose([Validators.required])], // i.e. Home, Office
      value: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }


  // skill group

  createSkill(): FormGroup {
    return this.fb.group({
      skill: [null, Validators.compose([Validators.required])],
      experience: [null, Validators.compose([Validators.required])],
      proficiency: [null, Validators.compose([Validators.required])]
    });
  }
  // add a contact form group
  addContact() {
    this.contactList.push(this.createContact());
  }
  addSkill() {
    this.skillList.push(this.createSkill());
  }

  // remove contact from group
  removeContact(index) {
    // this.contactList = this.form.get('contacts') as FormArray;
    this.contactList.removeAt(index);
  }

  // remove skill from group
  removeSkill(index) {
    // this.contactList = this.form.get('contacts') as FormArray;
    this.skillList.removeAt(index);
  }

  // triggered to change validation of value field type
  changedFieldType(index) {
    let validators = null;

    if (this.getContactsFormGroup(index).controls['type'].value === 'email') {
      validators = Validators.compose([Validators.required, Validators.email]);
    } 
    else if (this.getContactsFormGroup(index).controls['type'].value === 'date') {
      validators = Validators.compose([
        Validators.required// pattern for validating international phone number
      ]);
    }

    else {
      validators = Validators.compose([
        Validators.required,
        Validators.pattern('(01)[0-9]{9}') // pattern for validating international phone number
      ]);
    }

    this.getContactsFormGroup(index).controls['value'].setValidators(
      validators
    );
    this.getContactsFormGroup(index).controls['value'].updateValueAndValidity();
    return validators
  }

  // get the formgroup under contacts form array
  getContactsFormGroup(index): FormGroup {
    // this.contactList = this.form.get('contacts') as FormArray;
    const formGroup = this.contactList.controls[index] as FormGroup;
    return formGroup;
  }

  getSkillsFormGroup(index): FormGroup {
    // this.contactList = this.form.get('contacts') as FormArray;
    const formGroup = this.skillList.controls[index] as FormGroup;
    return formGroup;
  }

  // method triggered when form is submitted
  submit() {
    this.form.markAllAsTouched()
    
    Object.keys(this.form.controls).forEach(field => { // {1}
      const control = this.form.get(field);     
             // {2}
      control.markAsTouched({ onlySelf: true });       // {3}
    });


    console.log(this.form.value);
  }
  onReset() {
    // reset whole form back to initial state
    this.form.reset();
    this.skillFormGroup.clear();
    this.contactFormGroup.clear();

  }
  getForm() {
    return JSON.stringify(this.form.value)
  }
  getContacts() {
    return JSON.stringify(this.contactFormGroup.length)
  }

}
