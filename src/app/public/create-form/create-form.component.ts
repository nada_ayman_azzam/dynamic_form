import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {
  public form: FormGroup;
  public dynamicInputsList: FormArray;
  inputType;
  isSubmitted = false;
  constructor(private fb: FormBuilder) { }

  get dynamicInputFormGroup() {
    return this.form.get('dynamicInputs') as FormArray;
  }
  ngOnInit(): void {
    this.form = this.fb.group({
      dynamicInputs: this.fb.array([this.createDynamicInput()])
    });
    this.dynamicInputsList = this.form.get('dynamicInputs') as FormArray;

  }
  // contact formgroup
  createDynamicInput(): FormGroup {
    return this.fb.group({
      type: [null, Validators.compose([Validators.required])], // i.e Email, Phone
      name: [null, Validators.compose([Validators.required])], // i.e. Home, Office
      value: [null, Validators.compose([Validators.required])]
    });
  }

  // triggered to change validation of value field type
  changedFieldType(index) {
    let validators = null;

    if (this.getDynamicInputsFormGroup(index).controls['type'].value === 'email') {
      validators = Validators.compose([Validators.required, Validators.email]);

    } else if (this.getDynamicInputsFormGroup(index).controls['type'].value === 'date') {
      validators = Validators.compose([
        Validators.required// pattern for validating international phone number
      ]);
    }

    else {
      validators = Validators.compose([
        Validators.required,
        Validators.pattern('(01)[0-9]{9}') // pattern for validating international phone number
      ]);
    }

    this.getDynamicInputsFormGroup(index).controls['value'].setValidators(
      validators
    );

    this.getDynamicInputsFormGroup(index).controls['value'].updateValueAndValidity();
  }
  // get the formgroup under contacts form array
  getDynamicInputsFormGroup(index): FormGroup {
    // this.contactList = this.form.get('contacts') as FormArray;
    const formGroup = this.dynamicInputsList.controls[index] as FormGroup;
    return formGroup;
  }

  addDynamicInput(name, type) {
    // {2}
    if (name.value && type.value) {
      this.dynamicInputsList.push(this.createDynamicInput());
    }
    this.dynamicInputsList.markAllAsTouched()


  }

  removeDynamicInput(index) {
    this.dynamicInputsList.removeAt(index);

  }
  submit() {
    console.log(this.form.value.dynamicInputs.map(e => e.type))
    for (let index = 0; index < this.form.value.dynamicInputs.length; index++) {
      const element = this.form.value.dynamicInputs[index];
      console.log(element)
      if (element.type != null && element.name  != null) {
        this.isSubmitted = true;
        

      } else{
        this.isSubmitted = false;

      }

    }
    this.dynamicInputsList.markAllAsTouched()
  }

}
