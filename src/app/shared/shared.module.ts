import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialImportsModule } from './modules';
import { DynamicFieldDirective } from './directives';
import { ButtonComponent, CheckboxComponent, DateComponent, DynamicFormComponent, InputComponent, RadiobuttonComponent, SelectComponent } from './components';




const COMPONENTS = [
    InputComponent,
    ButtonComponent,
    SelectComponent,
    DateComponent,
    DynamicFormComponent,
    CheckboxComponent,
    RadiobuttonComponent
]
const DIRETCIVE = [
    // DynamicFieldDirective
]

@NgModule({
    declarations: [
        ...COMPONENTS
        , ...DIRETCIVE],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialImportsModule,
    ],
    providers: [],
    exports: [
        ...COMPONENTS,
        ...DIRETCIVE,

        MaterialImportsModule,

    ],
    entryComponents: [
        ...COMPONENTS
    ]
})
export class SharedModule { }
