import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UserAuthService } from '../services/api';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {
  constructor( private AuthUser:UserAuthService ) { }

   swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })

   afterSubmitToast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  afterSubmit(success,title){
    if(success === 'true'){
      this.afterSubmitToast.fire({
        icon: 'success',
        title: title
      })
    }
    else{
      this.afterSubmitToast.fire({
        icon: 'error',
        title: title
      })
    }
  }

  checkCurrentUser(){
      
  this.swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, Log Me Out!!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
     if(localStorage.getItem("user") && localStorage.getItem("user_token")){
        this.AuthUser.logOut();
        this.swalWithBootstrapButtons.fire(
          'logged Out !',
          'You Logged Out from User',
          'success'
        )
        
      }
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      this.swalWithBootstrapButtons.fire(
        'Cancelled',
        'Your imaginary file is safe :)',
        'error'
      )
    }
  })
  }

}
